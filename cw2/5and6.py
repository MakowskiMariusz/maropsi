class Calculator:
    def __init__ (self):
        print("Launching completed")
    def add(self, x,y):
        print(x+y)
    def subtract(self,x,y):
        print(x-y)
    def multiply(self,x,y):
        print(x*y)
    def divide(self,x,y):
        print(x/y)

class ScienceCalculator(Calculator):
    def __init__ (self):
        print("Uruchomiono kalkulator naukowy")
    def square(self, x):
        print(x*x)
    def cube(self,x):
        print(x*x*x)

#############
# RealitzationOfCalc

calc = Calculator()
calc.add(15,10)
calc.subtract(125,100)
calc.multiply(5,5)
calc.divide(125,5)
calc = ScienceCalculator()
calc.square(5)
calc.cube(3)

