def exc1(a_list = [], b_list = []):
    list = []
    for index, x in enumerate(a_list):
        if (index%2 == 0):
            list.append(x)
    for index, x in enumerate(b_list):
        if (index%2 != 0):
            list.append(x)
    return list
