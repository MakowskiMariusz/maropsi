def exc4(temp, temperature_type):
    if (temperature_type == 'C'):
        print(temp, "*C = ", temp, "*C")
    elif (temperature_type == 'F'):
        print(temp, "*C = ", 32+(9*temp)/5, "*F")
    elif (temperature_type == 'R'):
        print(temp, "*C = ", (9*(temp+273))/5, "*R")
    elif (temperature_type == 'K'):
        print(temp, "*C = ", temp+273, "*K")
    else: print("Wrong paramete, correct one is : C, F, R or K")